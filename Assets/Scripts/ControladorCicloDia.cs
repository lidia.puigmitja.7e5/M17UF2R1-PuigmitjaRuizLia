using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public enum CiclosDias
{
    Dia,
    Noche
};
public class ControladorCicloDia : MonoBehaviour
{
    [SerializeField] private Light2D luzGlobal;
    [SerializeField] private CicloDia[] ciclosDia;
    [SerializeField] private float tiempoPorCiclo;

    private float tiempoActualCiclo = 0;
    private float porcentajeCiclo;
    private int cicloActual = 0;
    private int cicloSiguiente = 1;

    private bool _changeDay = false;


    public delegate void LightOn();
    public static event LightOn LightOnn;

    public delegate void LightOff();
    public static event LightOff LightOfff;

    private static CiclosDias _cicloDias;
    public static CiclosDias CicloDias
    {
        get => _cicloDias;
    }

    public static ControladorCicloDia ControladorSingleton;

   
    private void Awake()
    {
        if (ControladorSingleton == null)
        {
            ControladorSingleton = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

       

    }
    private void Start()
    {
        luzGlobal.color = ciclosDia[0].colorCiclo;
        _cicloDias = CiclosDias.Dia;
       
    }

    private void Update()
    {
        if (GameManager.EstadoJuego == EstadoJuego.Running && GameManager.PantallasJuego != PantallasJuego.GameOver)
        {
            tiempoActualCiclo += Time.deltaTime;
            porcentajeCiclo = tiempoActualCiclo / tiempoPorCiclo;
            if (_changeDay == false)
            {
                if (tiempoActualCiclo >= (tiempoPorCiclo / 2))
                {
                    if (LightOfff != null && LightOnn != null)
                    {
                        switch (_cicloDias)
                        {
                            case CiclosDias.Dia:
                                _cicloDias = CiclosDias.Noche;
                                LightOnn();
                                break;
                            case CiclosDias.Noche:
                                LightOfff();
                                _cicloDias = CiclosDias.Dia;
                                break;
                        }
                    }

                    _changeDay = true;
                }
            }

            if (tiempoActualCiclo >= tiempoPorCiclo)
            {
                tiempoActualCiclo = 0;
                _changeDay = false;

                cicloActual = cicloSiguiente;


                if (cicloSiguiente + 1 > ciclosDia.Length - 1)
                {
                    cicloSiguiente = 0;

                }
                else
                {
                    cicloSiguiente += 1;
                }
            }
            //Cambiar color
            CambiarColor(ciclosDia[cicloActual].colorCiclo, ciclosDia[cicloSiguiente].colorCiclo);
        }

    }

    private void CambiarColor(Color colorActual, Color siguienteColor)
    {
        luzGlobal.color = Color.Lerp(colorActual, siguienteColor, porcentajeCiclo);
    }
}
