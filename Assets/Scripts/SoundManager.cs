using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioClip nivel1, nivel2, nivel3;
    private AudioSource audioSrc;

    // Start is called before the first frame update
    void Awake()
    {
        nivel1 = Resources.Load<AudioClip>("cancion5");
        nivel2 = Resources.Load<AudioClip>("cancion1");
        nivel3 = Resources.Load<AudioClip>("cancion6");
       
        audioSrc = gameObject.GetComponent<AudioSource>();

    }
    private void Start()
    {
        switch (GameManager.PantallasJuego)
        {
            case PantallasJuego.Nivel1:
                PlaySound("cancion5");
                break;
            case PantallasJuego.Nivel2:
                PlaySound("cancion1");
                break;
            case PantallasJuego.Nivel3:
                PlaySound("cancion6");
                break;

        }
    }


    public void PlaySound(string clip)
    {
        switch (clip)
        {
            case "cancion5":
                audioSrc.PlayOneShot(nivel1);
                break;
            case "cancion1":
                audioSrc.PlayOneShot(nivel2);
                break;
            case "cancion6":
                audioSrc.PlayOneShot(nivel3);
                break;
        

        }

    }
}
