using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Start is called before the first frame update
[CreateAssetMenu(fileName = "Weapon", menuName = "ScriptableObjects/Weapons/Weapon", order = 1)]
public class WeaponsDefaultSO : ScriptableObject
{
    public Projectil bulletPrefab; // Stores out Bullet Prefab
    public GameObject weaponPrefab;

    public string weaponName;       // weapon name e.g  plasma cannon

    public int totalBullets;           // amount of bullets I have e.g. 5
    public int totalMagazine;           // amount of magazine I have e.g. 5
    public int totalMagazineWeapon;    // amount of magazine per weapon e.g. 5
    public int magazineCapacity;    // amount of bullets per magazine e.g. 5
    public float reloadSpeed;       // time to reload   e.g 2.5f (2.5 seconds)
    public float fireRate;          // bullets shot per second 1f (1 per second)
    public float speedBullets;            // speed per bullet 
    public virtual void SetValues()
    {

        totalBullets = magazineCapacity;
        totalMagazine = totalMagazineWeapon;

    }



}




