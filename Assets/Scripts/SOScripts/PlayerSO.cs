using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "PlayerData", menuName = "ScriptableObjects/PlayerData", order = 1)]

public class PlayerSO : ScriptableObject
{
    public float MaxHealth;
    public float actualHealth;
    public string name;
    public int speed;
    public bool IsAlive;
    public Animator animator;
    public Vector3 currentPosition;

}
