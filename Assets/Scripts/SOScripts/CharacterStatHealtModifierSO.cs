using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterStatHealtModifierSO", menuName = "ScriptableObjects/CharacterStatHealtModifierSO", order = 6)]

public class CharacterStatHealtModifierSO : CharacterStateModifierSO
{
    public override void AffectCharacter(GameObject character, float val)
    {
        character.GetComponent<Player>().ItHeals(val);
       
    }
}
