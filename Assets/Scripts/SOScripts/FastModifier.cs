using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FastModifier", menuName = "ScriptableObjects/FastModifier", order = 9)]
public class FastModifier : CharacterStateModifierSO
{

    public override void AffectCharacter(GameObject character, float val)
    {
        character.GetComponent<Player>().FastFast(val);
    }

}
