using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "AturdirModifier", menuName = "ScriptableObjects/AturdirModifier", order = 8)]
public class AturdirModifier : CharacterStateModifierSO
{
    public override void AffectCharacter(GameObject character, float val)
    {
        character.GetComponent<Player>().IsAturdido(val);
    }
}
