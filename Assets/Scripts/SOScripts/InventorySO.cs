using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "InventorySO", menuName = "ScriptableObjects/InventorySO", order = 3)]
public class InventorySO : ScriptableObject
{
    [field: SerializeField]
    public List<ItemSO> inventoryItems;

    public int Size { get; private set; } = 4;

    public int ItemsToCharge = 4;


    public void AddItem(ItemSO item)
    {

        inventoryItems.Add(item);


    }
    public ItemSO GetItemAt(int itemIndex)
    {
        return inventoryItems[itemIndex];
    }

    public void RemoveItem(int itemIndex)
    {
        inventoryItems.RemoveAt(itemIndex);
        ItemsToCharge++;
    }

}
