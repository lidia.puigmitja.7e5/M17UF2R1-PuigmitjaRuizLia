using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponListScriptableObject", menuName = "ScriptableObjects/Weapons/WeaponList", order = 0)]
public class WeaponList : ScriptableObject
{
    public WeaponsDefaultSO TriposArcoirisValues;
    public WeaponsDefaultSO SpeedTruenoValues;
    public WeaponsDefaultSO MDM4morValues;
    public WeaponsDefaultSO BenzaXanaXValues;
    public WeaponsDefaultSO AntiPsikotikValues;

}
