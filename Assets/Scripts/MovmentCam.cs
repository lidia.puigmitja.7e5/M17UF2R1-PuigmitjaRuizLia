using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovmentCam : MonoBehaviour
{
    private Transform playerTrans;

    // Update is called once per frame
    void Update()
    {
        if (GameManager.PantallasJuego != PantallasJuego.GameOver)
        {
            playerTrans = GameObject.Find("Player(Clone)").GetComponent<Transform>();
            this.transform.position = new Vector3(playerTrans.position.x, playerTrans.position.y, this.transform.position.z);
        }
    }
}
