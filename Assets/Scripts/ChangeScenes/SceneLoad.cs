using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoad : MonoBehaviour
{
   
    public void GameOver()
    {
        if (GameManager.PlayerAlive == false)
        {
            SceneManager.LoadScene("FinalScene");
        }
    }
    
}
