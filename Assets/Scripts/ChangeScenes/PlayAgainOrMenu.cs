using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayAgainOrMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject panelWin;
    private Button _botton1;
    private Button _botton2;
    private TMP_Text _textFinal;
    private TMP_Text _textSala;
    // Start is called before the first frame update
    void Start()
    {
        string path = Application.dataPath + "/StreamingAssets/ranking.json";
        panelWin.SetActive(false);
        string sala = "";
        switch (GameManager.SalaYouDie)
        {
            case 0:
                sala = "Vivo hasta el final";
                break;
            case 1:
                sala = "Moriste en la sala uno, aprende a salir de fiesta.";
                break;
            case 2:
                sala = "No se te da del todo bien eso de salir por ah�, moriste en la sala 2.";
                break;
            case 3:
                sala = "Casi sobrevives a esta rave, moriste en la sala 3.";
                break;
            default:
                break;
        }
        CompareScore();
        string[] bestGame=ShowHistory.SelectBestGame(path);

        GameObject[] bestFilas = {
            GameObject.Find("name"), GameObject.Find("puntuacio"),
            GameObject.Find("data")
        };
        for (int i = 0; i < bestGame.Length; i++)
        {
            bestFilas[i].GetComponent<TMP_Text>().text = bestGame[i];
        }


        _textFinal =GameObject.Find("TextFinal").GetComponent<TMP_Text>();
        if (GameManager.PlayerAlive == false)
        {
        _textFinal.text = "Sorry "+GameManager.PlayerName+"\r\nYou Are Dead, too many drugs and fake friends.\nTotal friends you healt: "+GameManager.TotalKillEnemies+ "\nTotal RaveCoins you win: "+GameManager.RaveCoins;

        }else if(GameManager.PlayerAlive == true)
        {
            _textFinal.text = "The best rave is where you are " + GameManager.PlayerName + "\r\nYou've taken care of your friends and of yourself. Good job! Remember to drink water.\nTotal friends you healt: "+GameManager.TotalKillEnemies + "\nTotal RaveCoins you win: " + GameManager.RaveCoins;

        }
        _textSala = GameObject.Find("TextSala").GetComponent<TMP_Text>();
        _textSala.text = sala;
        _botton1 = GameObject.Find("PlayAgain").GetComponent<Button>();
        _botton2 = GameObject.Find("GoMenu").GetComponent<Button>();
        _botton1.onClick.AddListener(PlayAgainReset);
        _botton2.onClick.AddListener(GoToMenu);

    }


    void PlayAgainReset()
    {
         Resetear();
         SceneManager.LoadScene("LevelsProcedural");
        GameManager.EstadoJuego = EstadoJuego.Running;
        GameManager.PantallasJuego = PantallasJuego.Nivel1;

        
    }
  

    void Resetear()
    {
        GameManager.PlayerAlive = true;
        GameManager.Enemies = 0;
        GameManager.TotalKillEnemies = 0;
        GameManager.SalaYouDie= 0;


     }

    void GoToMenu()
    {
        Resetear();
        GameManager.PlayerName = null;
        SceneManager.LoadScene("MenuPrincipal");
    }

    void CompareScore()
    {
        string path = Application.dataPath + "/StreamingAssets/ranking.json";
        string[] bestgame= ShowHistory.SelectBestGame(path);
        string[] lastGame= ShowHistory.SelectLastGame(path);

        if (Convert.ToInt32(bestgame[1]) <= Convert.ToInt32(lastGame[1]))
        {
            panelWin.SetActive(true);
        }
    }


  }