using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeScene : MonoBehaviour
{
    [SerializeField]
    private string _scene;
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(LoadStartGame);
    }

    private void LoadStartGame()
    {
        SceneManager.LoadScene(_scene);
        GameManager.PantallasJuego = PantallasJuego.Nivel1;
    }
}
