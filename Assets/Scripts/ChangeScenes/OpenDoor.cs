using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenDoor : MonoBehaviour
{
    private Animator _animator;
    private bool _openDoor = false;
    private Collider2D _collider;
 
    // Start is called before the first frame update
    void Start()
    {
        _collider = GetComponent<Collider2D>();
        _animator =gameObject.GetComponent<Animator>();
        GameEvents.KilledAllEnemies += OpeningDoor;
       
    }

    // Update is called once per frame
    void OpeningDoor()
    {

       
            _openDoor = true;
            _animator.SetBool("OpenDoor", _openDoor);
            StartCoroutine(IsOpened(_animator.GetCurrentAnimatorStateInfo(0).length));
            _collider.isTrigger = true;
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            switch (GameManager.PantallasJuego)
            {
                case PantallasJuego.Nivel1:
                    GameManager.PantallasJuego = PantallasJuego.Nivel2;
                    GameManager.Enemies = 0;
                    GameManager.SetRaveCoins(100);
                    SceneManager.LoadScene("LevelsProcedural");
                    break;
                case PantallasJuego.Nivel2:
                    GameManager.PantallasJuego = PantallasJuego.Nivel3;
                    GameManager.Enemies = 0;
                    GameManager.SetRaveCoins(200);
                    SceneManager.LoadScene("LevelsProcedural");
                    break;
                case PantallasJuego.Nivel3:
                    GameManager.PantallasJuego = PantallasJuego.GameOver;
                    GameManager.Enemies = 0;
                    GameManager.SetRaveCoins(300);
                    SceneManager.LoadScene("FinalScene");
                    break;
            }
            
        }
    }

    public IEnumerator IsOpened(float t)
    {
        yield return new WaitForSeconds(t);
        _animator.SetBool("IsOpened", _openDoor);
    }
    private void OnDisable()
    {
        GameEvents.KilledAllEnemies -= OpeningDoor;

    }
}
