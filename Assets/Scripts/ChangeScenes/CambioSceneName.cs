using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CambioSceneName : MonoBehaviour
{
    private string _name;

    private GameObject _popUp;
    // Start is called before the first frame update
    void Start()
    {
        
        _popUp = GameObject.Find("PopUp");
        _popUp.SetActive(false);
        
        string path = Application.dataPath + "/StreamingAssets/ranking.json";
        bool filexists1 = ResultSave.IfFileExists(path);
        string testDate = ResultSave.GetDateTimeNow();
        if (filexists1 == false)
            ResultSave.CreateJsonFile(testDate, path);
        
        GetComponent<Button>().onClick.AddListener(LoadStartGame);
    }


    void LoadStartGame()
    {

        if (GameObject.Find("Name").GetComponent<TMP_Text>().text.Length > 3)
        {
            _name = GameObject.Find("Name").GetComponent<TMP_Text>().text;
            GameManager.PlayerName = _name;

            SceneManager.LoadScene("MenuPrincipal");
        }
        else
        {
            //Muestra el pop-up
            _popUp.SetActive(true);
            //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
            StartCoroutine(CloseAfterTime(1.5f));
        }
    }

    public IEnumerator CloseAfterTime(float t)
    {
        yield return new WaitForSeconds(t);
        _popUp.SetActive(false);
    }


}
