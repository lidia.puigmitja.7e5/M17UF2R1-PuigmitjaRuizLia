using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectil : MonoBehaviour
{
    private Rigidbody2D _rgbProjectil;
    private float _destroyDelay = 20f;


    private void Awake()
    {
        _rgbProjectil = GetComponent<Rigidbody2D>();
    }


    public void LaunchProjectil(Vector2 direction, float speed)
    {
        _rgbProjectil.velocity = direction*speed;
        Destroy(gameObject, _destroyDelay);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }
}
