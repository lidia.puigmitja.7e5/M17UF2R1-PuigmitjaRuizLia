using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BubbleBullets : MonoBehaviour
{
    [SerializeField]
    private GameObject _bullet;

    private float _timelife = 5f;

    [SerializeField]
    private float _timer=2f;

    private int _counter;
    [SerializeField]
    private int _maxCounter = 100;

    Vector2 _direction;

    // Start is called before the first frame update
    void Start()
    {
       
            StartCoroutine(BubbleBullet());
        
    }

    // Update is called once per frame
    void Update()
    {
        _direction = GetComponent<AimRotation>().GetDirectionPlayer();
    }



    IEnumerator BubbleBullet()
    {
        
            Transform eyeOne = this.gameObject.transform.GetChild(0);
            Transform eyeTwo = this.gameObject.transform.GetChild(1);

            for (int i = 0; i < _maxCounter; i++)
            {
                yield return new WaitForSeconds(_timer);
                _counter++;
            if (ControladorCicloDia.CicloDias == CiclosDias.Noche && GameManager.EstadoJuego == EstadoJuego.Running)
            {
                GameObject bulletOne = Instantiate(_bullet, eyeOne.transform.position, transform.rotation);
                bulletOne.GetComponent<Rigidbody2D>().AddForce(_direction, ForceMode2D.Impulse);
                Destroy(bulletOne, _timelife);
                GameObject bulletTwo = Instantiate(_bullet, eyeTwo.transform.position, transform.rotation);
                bulletTwo.GetComponent<Rigidbody2D>().AddForce(_direction, ForceMode2D.Impulse);
                Destroy(bulletTwo, _timelife);
            }



            }
        }
    
}
