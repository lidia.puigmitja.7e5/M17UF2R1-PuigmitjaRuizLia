using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsBubbleColision : MonoBehaviour
{
    [SerializeField]
    private float bulletDamage=2f;
    [SerializeField]
    private float _timelife = 0.5f;
    private Animator _animator;
    private bool _explosion = false;


    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _explosion = false;
        _animator.SetBool("explosion", _explosion);


    }

    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (ControladorCicloDia.CicloDias == CiclosDias.Noche && GameManager.EstadoJuego == EstadoJuego.Running)
        {
            switch (collision.gameObject.tag)
            {

                case "Player":
                    if (collision.gameObject.GetComponent<Player>().invulnerable == false)
                    {
                        collision.gameObject.GetComponent<Player>().IsDamage(bulletDamage);
                    }
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    Collider2D collider = gameObject.GetComponent<Collider2D>();
                    Rigidbody2D rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;
                case "bBombeta":
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    collider = gameObject.GetComponent<Collider2D>();
                    rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;
                case "bTriposa":
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    collider = gameObject.GetComponent<Collider2D>();
                    rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;
                case "bSpeed":
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    collider = gameObject.GetComponent<Collider2D>();
                    rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;
                case "bXanax":
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    collider = gameObject.GetComponent<Collider2D>();
                    rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;
                case "bAntiPsik":
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    collider = gameObject.GetComponent<Collider2D>();
                    rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;
                case "Enemy":
                    _explosion = true;
                    _animator.SetBool("explosion", _explosion);
                    collider = gameObject.GetComponent<Collider2D>();
                    rigthbody = gameObject.GetComponent<Rigidbody2D>();
                    rigthbody.constraints = RigidbodyConstraints2D.FreezePosition;
                    collider.isTrigger = true;
                    Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
                    _explosion = false;
                    break;

                default:
                    Destroy(gameObject, _timelife);
                    break;
            }

        }
        else
        {
            Destroy(gameObject, _timelife);
        }
       

    }




 
}
