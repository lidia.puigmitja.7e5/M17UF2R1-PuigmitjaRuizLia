using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimRotation : MonoBehaviour
{
    private Transform _playerTransform;
 

    // Update is called once per frame
    void Update()
    {
        
        
        transform.up = GetDirectionPlayer();

    }

    public Vector2 GetDirectionPlayer()
    {
        if (GameManager.PlayerAlive == true)
        {
        _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        Vector2 targetOrientation = _playerTransform.position - transform.position;
        return targetOrientation;
        }
        return Vector2.zero;
    }
}
