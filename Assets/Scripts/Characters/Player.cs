using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    private Rigidbody2D _rigidbody;
    private float _inputMovimientoX;
  
    private float _inputMovimientoY;
    private GameObject scream;

    [SerializeField]
    private PlayerSO playerSO;
    [SerializeField]
    private Transform shootPosition;

    private VerInventario verInventario;


    [SerializeField] private float _dashingTime = 0.2f;
    [SerializeField] private float _dashForce = 20f;
    [SerializeField] private float _timeCanDash = 1f;

    private bool _isDashing = false;
    private bool _canDash = true;
    public bool invulnerable = false;
    public bool moreFast = false;

    private void Awake()
    {
        actualHealth = MaxHealth;
        GameManager.PlayerAlive = true;
        LoadPlayerData();
        transform.position = currentPosition;
        verInventario= GameObject.Find("EquipoButon").GetComponent<VerInventario>();

    }



     void ProcesarMovimiento()
    {
        _inputMovimientoX = Input.GetAxis("Horizontal");
        _inputMovimientoY = Input.GetAxis("Vertical");

        if (_inputMovimientoX != 0 || _inputMovimientoY != 0)
        {
            if ((Input.GetKeyDown(KeyCode.Space) || (Input.GetKeyDown(KeyCode.RightShift)) && _canDash))
            {
                Vector2 direction = (new Vector2(_inputMovimientoX, _inputMovimientoY)).normalized;
                StartCoroutine(Dash(direction));
            }
            else
            {
                _rigidbody.velocity = new Vector2(_inputMovimientoX * speed, _inputMovimientoY * speed);
            }

        } 



    }

    void ProcesarGiro()
    {
        Vector2 mouseWorldSpace = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 direction = mouseWorldSpace - (Vector2)transform.position;
        transform.up = -direction;

    }



    public void ItHeals(float hpToHeal)
    {
        if (actualHealth + hpToHeal > MaxHealth)
            actualHealth = MaxHealth;
        else
            actualHealth += hpToHeal;
    }




    public override void Die()
    {
        Debug.Log(gameObject.name + " muri�");
        int sala=0;
        switch (GameManager.PantallasJuego)
        {
            case PantallasJuego.Nivel1:
                sala = 1; 
                break;
            case PantallasJuego.Nivel2:
                sala = 2;
                break;
            case PantallasJuego.Nivel3:
                sala = 3;
                break;
            default:
                break;
        }


        GameManager.SalaYouDie = sala;
        GameManager.PlayerAlive = false;
        GameManager.PantallasJuego = PantallasJuego.GameOver;
        StartCoroutine(ChargeSceneGameOver());
    }

    public override void IsDamage(float damage)
    {
        if (_isDashing == false)
        {
        actualHealth -= damage;
        if (actualHealth <= 0)
            Die();
        }
        
    }

    public void LoadPlayerData()
    {
        playerSO.name = GameManager.PlayerName;
        this.name = playerSO.name;
        this.IsAlive = playerSO.IsAlive;
        this.speed = playerSO.speed;
        this.MaxHealth = playerSO.MaxHealth;
        this.actualHealth = playerSO.actualHealth;
        this.animator = playerSO.animator;
        this.currentPosition=playerSO.currentPosition;

    }



    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
      

    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.EstadoJuego != EstadoJuego.InventoryTime)
        {
            if (_isDashing==false)
            {
                ProcesarMovimiento();

            }

            ProcesarGiro();

      
        }

        if (Input.GetKeyDown(KeyCode.I))
        {
            verInventario.VerElInventario();
        }

        if (invulnerable == true)
        {
            Debug.Log("Soc invulnerable");
        }
        if (moreFast == true)
        {
            speed = 10f;
        }
        else
        {
            speed = playerSO.speed;
        }

    }

    public void IsInvulnerable(float time)
    {
        StartCoroutine(Invulnerable(time));
    }

    private IEnumerator ChargeSceneGameOver()
    {
        scream = Instantiate(Resources.Load("Scream")) as GameObject;
        Destroy(scream, 0.4f);
        Destroy(gameObject, 0.6f);
        yield return new WaitForSeconds(0.5f);
        GetComponent<SceneLoad>().GameOver();

    }

    private IEnumerator Dash( Vector2 direcction)
    {
        if ((_inputMovimientoX != 0 || _inputMovimientoY != 0) && _canDash)
            _isDashing = true;
        _canDash = false;
        _rigidbody.velocity = new Vector2(_inputMovimientoX * _dashForce, _inputMovimientoY * _dashForce);
        yield return new WaitForSeconds(_dashingTime);
        _isDashing = false;
        yield return new WaitForSeconds(_timeCanDash);
        _canDash = true;

    }

    public void FastFast(float time)
    {
        StartCoroutine(Fast(time));
    }
    private IEnumerator Fast(float time)
    {
       moreFast= true;
        yield return new WaitForSeconds(time);
        moreFast = false;


    }

    public void IsAturdido(float time)
    {
        StartCoroutine(Aturdidor(time));
    }
    private IEnumerator Aturdidor(float time)
    {
        PunkyScreamExplote.aturdido = true;
        yield return new WaitForSeconds(time);
        PunkyScreamExplote.aturdido = false;


    }

    private IEnumerator Invulnerable(float time)
    {
        invulnerable = true;
        yield return new WaitForSeconds(time);
        invulnerable = false;
       

    }


}