using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using static UnityEngine.GraphicsBuffer;

public class PunkyScreamExplote : Enemy
{

    private bool _explode;
    private Transform jugador;
    Vector2 _direction;
    [SerializeField] private GameObject life;

    private GameObject scream;

    public static bool aturdido=false;

    public override void Die()
    {
        Debug.Log(gameObject.name + " muri�");
        Destroy(gameObject, animator.GetCurrentAnimatorStateInfo(0).length);
        GameManager.Enemies--;
        GameManager.TotalKillEnemies++;
        GameManager.SetRaveCoins(30);
        Destroy(scream, animator.GetCurrentAnimatorStateInfo(0).length - 0.01f);

        int num = Random.Range(0, 20);
        if (num <= 4)
        {
            Instantiate(life, transform.position, transform.rotation);
        }
        Debug.Log(GameManager.Enemies);

    }

    public override void IsDamage(float damage)
    {
        actualHealth -= damage;
        if (actualHealth <= 0)
            Die();
    }

    private void Awake()
    {
        MaxHealth = 30;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        _explode = false;
        animator.SetBool("explode", _explode);
        speed = 2f;
        actualHealth = MaxHealth;
        name = "PunkyScreamExplote";
        currentPosition = transform.position;
        IsAlive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.PantallasJuego != PantallasJuego.GameOver)
        {
if (ControladorCicloDia.CicloDias == CiclosDias.Noche && GameManager.EstadoJuego == EstadoJuego.Running)
        {
            jugador = GameObject.FindGameObjectWithTag("Player").transform;

            FollowPlayer();
            if (aturdido == true)
            {
                speed = 0.5f;
            }
            else
            {
                speed = 2f;
            }

        }

        }
        



    }
    public void FollowPlayer()
    {

        if (GameManager.PlayerAlive == true)
        {
            _direction = GetComponent<AimRotation>().GetDirectionPlayer();
            transform.position = Vector3.MoveTowards(transform.position, jugador.position, speed * Time.deltaTime);
        }


    }





    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ControladorCicloDia.CicloDias == CiclosDias.Noche && GameManager.EstadoJuego == EstadoJuego.Running)
        {
            switch (collision.gameObject.tag)
            {

                case "Player":
                        scream = Instantiate(Resources.Load("Scream")) as GameObject;
                        
                        _explode = true;
                        animator.SetBool("explode", _explode);
                        Die();
                    if (collision.gameObject.GetComponent<Player>().invulnerable == false)
                    {
                        GameObject Player = collision.gameObject;
                        Player.GetComponent<Player>().IsDamage(5);
                        Vector2 direction = (Vector2)Player.transform.position - (Vector2)transform.position;
                        Player.GetComponent<Rigidbody2D>().AddForce(direction, ForceMode2D.Impulse);
                       
                    }
                    break;
                case "bBombeta":
                    gameObject.GetComponent<Enemy>().IsDamage(5);

                    break;
                case "bTriposa":
                    gameObject.GetComponent<Enemy>().IsDamage(3);

                    break;
                case "bSpeed":
                    gameObject.GetComponent<Enemy>().IsDamage(2);

                    break;
                case "bXanax":
                    gameObject.GetComponent<Enemy>().IsDamage(7);

                    break;
                case "bAntiPsik":
                    gameObject.GetComponent<Enemy>().IsDamage(10);

                    break;


                default:
                    break;
            }
        }
    }
}
