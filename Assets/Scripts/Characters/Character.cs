using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Character : MonoBehaviour
{

    public float MaxHealth;
    public float actualHealth;
    public string name;
    public float speed;
    public Vector3 currentPosition;
    public bool IsAlive;
    public Animator animator;

 

    public abstract void IsDamage(float damage);

    public abstract void Die();

}
