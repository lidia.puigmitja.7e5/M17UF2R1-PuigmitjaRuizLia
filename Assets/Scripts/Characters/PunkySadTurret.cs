using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PunkySadTurret : Enemy
{
    [SerializeField] private GameObject life;
    private GameObject scream;
    private bool isDiyng = false;
    public override void Die()
    {
        Debug.Log(gameObject.name + " muri�");
        scream = Instantiate(Resources.Load("Scream")) as GameObject;
        Destroy(scream, 0.3f);
        GameManager.Enemies--;
        GameManager.TotalKillEnemies++;
        GameManager.SetRaveCoins(50);

        int num = Random.Range(0, 20);
        if (num <= 4)
        {
            Instantiate(life, transform.position, transform.rotation);
        }
        Destroy(gameObject, 0.4f);
        Debug.Log(GameManager.Enemies);
    }

    public override void IsDamage(float damage)
    {
        if (isDiyng == false)
        {
            actualHealth -= damage;
            if (actualHealth <= 0)
            {
                Die();
                isDiyng = true;
            }
        }
   

    }

    private void Awake()
    {
        MaxHealth = 40;
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        actualHealth = MaxHealth;
        name = "PunkySadTurret";
        currentPosition = transform.position;
        IsAlive = true;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ControladorCicloDia.CicloDias == CiclosDias.Noche && GameManager.EstadoJuego == EstadoJuego.Running)
        {
            switch (collision.gameObject.tag)
            {

                case "Player":
                    if (collision.gameObject.GetComponent<Player>().invulnerable == false)
                    {
                        collision.gameObject.GetComponent<Player>().IsDamage(3);
                    }
                    gameObject.GetComponent<Enemy>().IsDamage(3);
                    break;
                case "bBombeta":
                    gameObject.GetComponent<Enemy>().IsDamage(10);

                    break;
                case "bTriposa":
                    gameObject.GetComponent<Enemy>().IsDamage(8);

                    break;
                case "bSpeed":
                    gameObject.GetComponent<Enemy>().IsDamage(7);

                    break;
                case "bXanax":
                    gameObject.GetComponent<Enemy>().IsDamage(3);

                    break;
                case "bAntiPsik":
                    gameObject.GetComponent<Enemy>().IsDamage(5);

                    break;


                default:
                    break;
            }
        }
    }
}
