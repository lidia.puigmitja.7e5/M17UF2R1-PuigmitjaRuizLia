using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Matriz : MonoBehaviour
{

    public int x, y;
    public int[,] matrizJuego;
    public int[,] matrizBlokes;

    public GameObject[] boskes;
    public GameObject player;
    public GameObject puerta;
    public GameObject tienda;
    public GameObject dj;

    public GameObject bloke;


    public static Matriz AutoCreateMatrizSingleton;


    private void Awake()
    {
        if (AutoCreateMatrizSingleton == null)
        {
            AutoCreateMatrizSingleton = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }



        CrearMundo();


    }

    public void CrearMundo()
    {

        switch (GameManager.PantallasJuego)
        {
            case PantallasJuego.Nivel1:
                x = 5;
                y = 5;
                break;

            case PantallasJuego.Nivel2:
                x = 10;
                y = 10;
                break;

            case PantallasJuego.Nivel3:
                x = 15;
                y = 15;
                break;
        }

        matrizJuego = new int[x, y];
        matrizBlokes= new int[x, y]; 
        TrazarRuta();
        DeterminarBlokes();
        CrearBlokes();
    }

    void TrazarRuta()
    {
        int puntoInicio = Random.Range(0, x);
        int puntoFin = Random.Range(0, x);

        int[] puntoFlotante = { puntoInicio, y - 1 };
        while (!(puntoFlotante[0] == puntoFin && puntoFlotante[1] == 0))
        {
            int movimiento;
            bool repetir=false;
            do
            {
                repetir = false;
                movimiento = Random.Range(1, 4);

                if (movimiento == 1)
                {
                    puntoFlotante[0]++;
                    if (puntoFlotante[0] >= x)
                    {
                        puntoFlotante[0]--;
                        repetir= true;
                    }
                }
                else if (movimiento == 2)
                {
                    puntoFlotante[1]--;
                    if (puntoFlotante[1] <0)
                    {
                        puntoFlotante[1]++;
                        repetir = true;
                    }
                }
                else if (movimiento == 3)
                {
                    puntoFlotante[0]--;
                    if (puntoFlotante[0] < 0)
                    {
                        puntoFlotante[0]++;
                        repetir = true;
                    }
                }
            } while (repetir);
            matrizJuego[puntoFlotante[0], puntoFlotante[1]] = 3;

        }

        int[] habitacion = { Random.Range(0, x), Random.Range(1, y-1) };

        matrizJuego[habitacion[0], habitacion[1]] = 4;
        Instantiate(dj, new Vector3(habitacion[0]*10, habitacion[1]*10), Quaternion.identity);
        Instantiate(tienda, new Vector3((habitacion[0] * 10)+3.14f, (habitacion[1] * 10)+3.49f), Quaternion.identity);
        //Conectar habitacion
        if (ValorBloke(habitacion[0], habitacion[1])==0)
        {
            int columna = -1;
            for (int i = 0; i < x; i++)
            {
                if (matrizJuego[i, habitacion[1]] !=0 && i != habitacion[0])
                {
                    columna = i;
                    break;
                }
            }
            if (columna > habitacion[0])
            {
                for (int i = habitacion[0]+1; i < columna; i++)
                {
                    matrizJuego[i, habitacion[1]] = 3;
                }
            }
            else
            {
                for (int i = columna; i < habitacion[0]; i++)
                {
                    matrizJuego[i, habitacion[1]] = 3;
                }
            }

        }

        matrizJuego[puntoInicio, y - 1] = 1;
        matrizJuego[puntoFin, 0] = 2;

        player=Instantiate(player, new Vector3(puntoInicio * 10, (y - 1) * 10), Quaternion.identity);
        player.transform.position = new Vector3(puntoInicio * 10, (y - 1) * 10);
        puerta = Instantiate(puerta, new Vector3(puntoFin * 10, 0), Quaternion.identity);

    }
    void DeterminarBlokes()
    {
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
               matrizBlokes[i, j] = ValorBloke(i,j);
            }
        }
    }

    public int ValorBloke(int i, int j)
    {
        int resultado = 0;
        resultado += 1 * HayBlokeONo(i, j + 1);
        resultado += 2 * HayBlokeONo(i + 1, j);
        resultado += 4 * HayBlokeONo(i, j - 1);
        resultado += 8 * HayBlokeONo(i - 1, j);
        return resultado;
    }

    void CrearBlokes()
    {
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                if (matrizJuego[i, j] != 0)
                {
                    
                    GameObject bl= Instantiate(bloke, new Vector3(i * 10, j * 10, 0), Quaternion.identity) as GameObject;
                    bl.GetComponent<Blokes>().Inicializar(matrizBlokes[i,j]);

                    int num = UnityEngine.Random.Range(0, 5);
                    if (num != 4){
                        Instantiate(boskes[num], new Vector3(i * 10, j * 10, 0), Quaternion.identity);
                    }
                    
                }
               
            }
        }
    }


    public int HayBlokeONo(int _x, int _y)
    {
        if(_x<0 || _y<0 || _x>=x || _y >= y)
        {
            return 0;
        }
        if (matrizJuego[_x, _y] == 0)
        {
            return 0;
        }
        return 1;
    }

    
    private void OnDrawGizmos()
    {
        if (matrizJuego == null)
        {
            return;
        }
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                if (matrizJuego[i, j] == 0)
                {
                    Gizmos.color = Color.black;
                }
                else if (matrizJuego[i, j] == 1)
                {
                    Gizmos.color = Color.green;
                }
                else if (matrizJuego[i, j] == 2)
                {
                    Gizmos.color = Color.red;
                }
                else if (matrizJuego[i, j] == 3)
                {
                    Gizmos.color = Color.yellow;
                }
                else if (matrizJuego[i, j] == 4)
                {
                    Gizmos.color = Color.blue;
                }
                // Gizmos.DrawCube(new Vector3(i * 10, j * 10, 0), Vector3.one * 10);
            }
        }
    }
}
