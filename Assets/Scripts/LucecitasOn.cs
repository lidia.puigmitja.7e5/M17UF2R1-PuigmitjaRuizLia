using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class LucecitasOn : MonoBehaviour
{

    void Start()
    {
        ControladorCicloDia.LightOnn += OpenLight;

    }


    void OpenLight()
    {
        this.gameObject.GetComponent<Light2D>().intensity = 1;

    }

    private void OnDisable()
    {
        ControladorCicloDia.LightOnn -= OpenLight;

    }
}
