using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RaveCoinsUI : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<TMP_Text>().text = GameManager.RaveCoins.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<TMP_Text>().text = GameManager.RaveCoins.ToString();

    }
}
