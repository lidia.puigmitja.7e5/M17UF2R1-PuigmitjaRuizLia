using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelDescriptionUI : MonoBehaviour
{
    [SerializeField]
    private Button buttonCloseItem;
    [SerializeField]
    private GameObject panelHelp;

    // Update is called once per frame
    void Start()
    {
        buttonCloseItem.onClick.AddListener(CloseDescriptionPanel);
    }

    void CloseDescriptionPanel()
    {
        gameObject.SetActive(false);
        panelHelp.SetActive(true);
    }
}
