using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigurationPanel : MonoBehaviour
{
    [SerializeField]
    private Button buttonCloseConfig;
   
    // Start is called before the first frame update
    void Start()
    {
        buttonCloseConfig.onClick.AddListener(CloseConfigPanel);

    }

    void CloseConfigPanel()
    {
        gameObject.SetActive(false);
    }
}
