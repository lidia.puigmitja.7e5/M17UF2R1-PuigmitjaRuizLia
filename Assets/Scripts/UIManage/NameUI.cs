using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NameUI : MonoBehaviour
{
    [SerializeField]
    private GameObject helpPopUp;
    [SerializeField]
    private GameObject itemDescription;
    [SerializeField]
    private Button buttonHelp;
    [SerializeField]
    private Button buttonConfig;
    [SerializeField]
    private GameObject panelConfig;

    // Start is called before the first frame update

    private void Awake()
    {
        panelConfig.SetActive(false);
        helpPopUp.SetActive(false);
        itemDescription.SetActive(false);  
    }

    void Start()
    {
        helpPopUp.SetActive(false);
        gameObject.GetComponent<TMP_Text>().text = GameManager.PlayerName;
        buttonHelp.onClick.AddListener(ActivarPanelAyuda);
        buttonConfig.onClick.AddListener(ActivarPanelConfig);
    }

   private void ActivarPanelAyuda()
   {
        helpPopUp.SetActive(true);
    }
    private void ActivarPanelConfig()
    {
        panelConfig.SetActive(true);
    }
}
