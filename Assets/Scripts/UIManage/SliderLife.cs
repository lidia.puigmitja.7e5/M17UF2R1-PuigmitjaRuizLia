using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderLife : MonoBehaviour
{
    private Slider _sliderLife;
    private Player _player;
    private float _vida;
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        _sliderLife= GetComponent<Slider>();
        _sliderLife.maxValue = _player.MaxHealth;
        _sliderLife.value = _sliderLife.maxValue;
    }

    // Update is called once per frame
    void Update()
    {
        _vida = _player.actualHealth;
        _sliderLife.value = _vida;
    }
}
