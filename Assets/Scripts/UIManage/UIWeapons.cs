using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIWeapons : MonoBehaviour
{

   private WeaponControl weaponControl;

    [SerializeField] private TMP_Text balasTotal;
    [SerializeField] private TMP_Text cargadorTotal;
    [SerializeField] private TMP_Text nombreArma;

    private void Start()
    {
        weaponControl = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<WeaponControl>();
    }

    // Update is called once per frame
    void Update()
    {
        balasTotal.text = weaponControl.weapon.GetTotalBullets().ToString();
        cargadorTotal.text = weaponControl.weapon.GetTotalMagazine().ToString();
        nombreArma.text = weaponControl.GetCurrentState().ToString();

    }
}
