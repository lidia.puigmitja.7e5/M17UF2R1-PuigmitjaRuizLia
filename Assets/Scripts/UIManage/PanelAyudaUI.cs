using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelAyudaUI : MonoBehaviour
{
    [SerializeField]
    private GameObject itemDescription;
    [SerializeField]
    private Button buttonItem;
    [SerializeField]
    private Button buttonCloseHelp;

    void Start()
    {
        buttonItem.onClick.AddListener(OpenItemDescription);
        buttonCloseHelp.onClick.AddListener(CloseHelpPanel);
    }

    void CloseHelpPanel()
    {
        gameObject.SetActive(false);
    }
    void OpenItemDescription()
    {
        gameObject.SetActive(false);
        itemDescription.SetActive(true);
        
    }
}
