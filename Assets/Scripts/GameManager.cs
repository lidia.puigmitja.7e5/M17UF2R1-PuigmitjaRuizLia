using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.SocialPlatforms.Impl;
using static WeaponControl;

public enum EstadoJuego
{
    Running,
    InventoryTime,
    Pause
};
public enum PantallasJuego
{
    Nivel1,
    Nivel2,
    Nivel3,
    GameOver
};

public class GameManager : MonoBehaviour
{

    private InventorySO inventory;
    private static EstadoJuego _estadoJuego;
    public static EstadoJuego EstadoJuego
    {
        get => _estadoJuego;
        set=> _estadoJuego = value;
    }

    private static PantallasJuego _pantallasJuego;
    public static PantallasJuego PantallasJuego
    {
        get => _pantallasJuego;
        set => _pantallasJuego = value;
    }
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }
    private static bool _playerAlive;
    public static bool PlayerAlive
    {
        get => _playerAlive;
        set => _playerAlive = value;
    }


    private static string _playerName;
    public static string PlayerName
    {
        get => _playerName;
        set => _playerName = value;
    }

    private static int _raveCoins= ShowHistory.SelectRaveCoins(Application.dataPath + "/StreamingAssets/Ranking.json");
    public static int RaveCoins
    {
        get => _raveCoins;
        set => _raveCoins = value;
    }

    public static void SetRaveCoinsRest(int ravecoinsGastadas)
    {
        RaveCoins= RaveCoins-ravecoinsGastadas;
    }

    private static int enemies;
    public static int Enemies
    {
        get => enemies;
        set => enemies = value;
    }

    private static int _totalKillEnemies=0;
    public static int TotalKillEnemies
    {
        get => _totalKillEnemies;
        set=> _totalKillEnemies = value;
    }
    private static int _salaYouDie = 0;
    public static int SalaYouDie
    {
        get => _salaYouDie;
        set => _salaYouDie = value;
    }


    private static Transform _playerTransform;

    public static void SetRaveCoins(int addRaveCoins) => _raveCoins += addRaveCoins;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
        enemies = 0;
        _raveCoins = 0;

        _playerTransform = GameObject.FindWithTag("Player").transform;
        inventory=GameObject.Find("Equipo").GetComponent<InventorySO>();  
        inventory.ItemsToCharge = 4;

    }




    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Singleton Update");
        _raveCoins = 0;
        
    }


    private void Update()
    {
       
    }


}
