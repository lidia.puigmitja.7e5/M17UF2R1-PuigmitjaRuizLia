using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerInventario : MonoBehaviour
{
    [SerializeField]
    private GameObject equipoUI;
    [SerializeField]
    private GameObject tiendaUI;
    bool _isOpened = false;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Button>().onClick.AddListener(VerElInventario);
    }

    public void VerElInventario()
    {
        
        if (_isOpened==false)
        {
            equipoUI.SetActive(true);
            equipoUI.GetComponent<Inventory>().Initialize();
            equipoUI.GetComponent<Inventory>().CargarInventario();
            GameManager.EstadoJuego = EstadoJuego.InventoryTime;
            _isOpened = true;
        }
        else if ( _isOpened == true)
        {
            equipoUI.SetActive(false);
            GameManager.EstadoJuego = EstadoJuego.Running;
            _isOpened = false;
        }
    }

   
}
