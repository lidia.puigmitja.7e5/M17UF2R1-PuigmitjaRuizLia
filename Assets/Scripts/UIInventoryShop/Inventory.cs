using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    private Transform panelEquipo;
    [SerializeField]
    private Transform itemUI;
    public InventorySO playerItems;

    private GameObject player;

    private TMP_Text quantity;

    private Image imatge;

    private void Awake()
    {
        panelEquipo = transform.Find("PanelEquipo");
        itemUI.gameObject.SetActive(false);
        if (playerItems.inventoryItems.Count == 0)
        {
            playerItems.ItemsToCharge = 4;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
        //Me pasa lo mismo q con la Tienda, no tiene sentido.. enfin....
        CargarInventario();
        
    }

    public void CargarInventario()
    {
        for (int i = 0; i < playerItems.inventoryItems.Count; i++)
        {

            CreateItemInventory(playerItems.inventoryItems[i], i);


        }
    }
    public void Initialize()
    {
        foreach (Transform item in panelEquipo)
        {
            Destroy(item.gameObject);
        }

    }



    private void CreateItemInventory(ItemSO item, int indexList)
    {
        Debug.Log(indexList);
        Transform itemUITransform = Instantiate(itemUI, panelEquipo);
        itemUITransform.gameObject.SetActive(true);

        quantity = itemUI.GetComponentInChildren<TMP_Text>();
        imatge = itemUI.transform.GetChild(1).GetComponent<Image>();

        quantity.text = "01";
        if(indexList+1 == playerItems.inventoryItems.Count)
        {
            imatge.sprite = playerItems.inventoryItems[0].ItemImage;
        }
        else
        {
            imatge.sprite = playerItems.inventoryItems[indexList+1].ItemImage;
        }
        
     
        itemUITransform.GetComponent<Button>().onClick.AddListener(() =>
        {
            player = GameObject.FindGameObjectWithTag("Player");
            IItemAction itemAction= item as IItemAction;
            if(itemAction != null)
            {
                itemAction.PerformAction(player);
            }
            IDestroyableItem itemDestroy = item as IDestroyableItem;
            if(itemDestroy!= null)
            {
                playerItems.RemoveItem(indexList);
                Initialize();
                //Me pasa lo mismo q con la Tienda, no tiene sentido.. enfin....
                CargarInventario();
            }
        });




    }

    public bool BoughtItem(ItemSO item)
    {
        bool noHaySitio;
        if (playerItems.ItemsToCharge > 0 && playerItems.ItemsToCharge < 5)
        {
            Debug.Log("HOLA COMPRO: " + item.Name);
            playerItems.AddItem(item);
            playerItems.ItemsToCharge -= 1;
            CreateItemInventory(item, item.ID);
            noHaySitio = false;
        }
        else
        {
            noHaySitio = true;
        }

        return noHaySitio;
    }

    public bool TrySpendGoldAmount(int raveCoins)
    {
        if (GameManager.RaveCoins >= raveCoins)
        {
            if (playerItems.ItemsToCharge >= 0 && playerItems.ItemsToCharge < 5)
            {
                GameManager.SetRaveCoinsRest(raveCoins);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }

    }
}
