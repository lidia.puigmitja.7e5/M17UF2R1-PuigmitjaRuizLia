using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class Tienda : MonoBehaviour
{
    private Transform panelTienda;
    [SerializeField]
    private Transform itemUI;
    public List<ItemSO> SellingItems;

    private bool _noEnoughCoind;
    private bool _noEnoughPlace;

    [SerializeField]
    private TMP_Text title;
    [SerializeField]
    private TMP_Text description;

    [SerializeField]
    private Inventory inventoryPlayer;

    private TMP_Text precio;

    private Image imatge;

    [SerializeField]
    private Button butonDescription;
    [SerializeField]
    private GameObject _popUp;
    [SerializeField]
    private GameObject _popUpBuy;

    int index = 0;
    bool changeDesc = false;
    private void Awake()
    {
        panelTienda = transform.Find("PanelTienda");
        itemUI.gameObject.SetActive(false);
    }
    private void Start()
    {
        for (int i = 1; i < SellingItems.Count; i++)
        {
            CreateItemButton(SellingItems[i], i);
        }   
        butonDescription.GetComponent<Button>().onClick.AddListener(() =>
        {
            changeDesc = true;
            if (changeDesc == true)
            {
                title.text = SellingItems[index].Name;
                description.text = SellingItems[index].Description;
                
                index++;
                if (index == SellingItems.Count-1)
                {
                    index = 0;
                }
                Debug.Log(index);
                changeDesc = false;
            }
            
        });
    }

    //Es muy bizarro pero se me instancia el ultimo item de la lista antes que el primero, para seguir recorriendo la lista,
    //pero en la descripcion me ponia los datos correctos de la lista por lo que no cuadraba la imagen con la descripcion. 
    //Despues de muchas horas de curro perdido porque no tiene sentido ya que en el Debug me sale el item de la lista pero el Sprite y el precio no,
    //encontre la solucion de a�adir un item igual que el primero de la lista y empezar a recorrer la lista desde la posicion 1, restando el indice de
    //la posicion de la lista en la descripcion y compra del item para que cuadre todo :'D 

    private void CreateItemButton(ItemSO item, int indexList)
    {

        Transform itemUITransform = Instantiate(itemUI, panelTienda);
        itemUITransform.gameObject.SetActive(true);

        precio = itemUI.GetComponentInChildren<TMP_Text>();
        imatge = itemUI.transform.GetChild(1).GetComponent<Image>();

        precio.text = item.Price.ToString();
        imatge.sprite = item.ItemImage;

        
     


        itemUITransform.GetComponent<Button>().onClick.AddListener(() =>
                   {
                       TryBuyItem(SellingItems[indexList - 1]);
                       if (_noEnoughCoind == true || _noEnoughPlace == true)
                       {
                           //Muestra el pop-up
                           _popUp.SetActive(true);
                           //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
                           StartCoroutine(CloseAfterTime(1.5f, _popUp));

                       }
                       else
                       {
                           //Muestra el pop-up
                           _popUpBuy.SetActive(true);
                           //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
                           StartCoroutine(CloseAfterTime(1.5f, _popUpBuy));
                       }


                   });




    }

    private void TryBuyItem(ItemSO item)
    {
        Debug.Log(item.Name);
        Debug.Log(item.Price);
        if (inventoryPlayer.TrySpendGoldAmount(item.Price))
        {

            _noEnoughPlace = inventoryPlayer.BoughtItem(item);
            Debug.Log(_noEnoughPlace);
            _noEnoughCoind = false;
        }
        else
        {
            _noEnoughCoind = true;
        }

    }

    public IEnumerator CloseAfterTime(float t, GameObject popUp)
    {
        yield return new WaitForSeconds(t);
        popUp.SetActive(false);
    }



}
