using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerTienda : MonoBehaviour
{
   private GameObject tiendaUI;
   private GameObject equipoUI;
   
    // Start is called before the first frame update
    void Start()
    {
        tiendaUI = GameObject.Find("TiendaObjetos");
        equipoUI = GameObject.Find("Equipo");
        tiendaUI.SetActive(false);
        equipoUI.SetActive(false);
    }

    private void Update()
    {
       
    }

   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            ToggleTienda(1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        ToggleTienda(0);
    }

    private void ToggleTienda(int a = 0)
    {
        if (a == 1)
        {
            tiendaUI.SetActive(true);
            equipoUI.SetActive(true);
            GameManager.EstadoJuego = EstadoJuego.Pause;
        }
        else
        {
            tiendaUI.SetActive(false);
            equipoUI.SetActive(false);
            GameManager.EstadoJuego = EstadoJuego.Running;
        }
    }
}
