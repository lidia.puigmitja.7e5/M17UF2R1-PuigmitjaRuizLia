using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShopController
{
    bool BoughtItem(ItemSO item);
    bool TrySpendGoldAmount(int goldCoins);

}
