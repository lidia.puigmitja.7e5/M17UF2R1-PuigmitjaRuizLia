using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public static Spawner SpawnerSingleton;

    private Vector3 _posicionInicial;
    public GameObject[] EnemiesPrefab = new GameObject[2];
    private int _num;
    private int _enemyMax = 0;


    private void Awake()
    {
        if (SpawnerSingleton == null)
        {
            SpawnerSingleton = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }

        //CreateWorld();


    }

    private void Start()
    {
        CheckLevel();
        while (_enemyMax != 0)
        {
            SpawnObject();
            _enemyMax--;

        }
        Destroy(gameObject);
    }
    private void CheckLevel()
    {
        switch (GameManager.PantallasJuego)
        {
            case PantallasJuego.Nivel1:
                _enemyMax = 7;
                break;
            case PantallasJuego.Nivel2:
                _enemyMax = 13;
                break;
            case PantallasJuego.Nivel3:
                _enemyMax = 17;
                break;
        }
        GameManager.Enemies = _enemyMax;
    }


    void SpawnObject()
    {

        _num = Random.Range(0, EnemiesPrefab.Length);
        Instantiate(EnemiesPrefab[_num], DondeSpawnear(), transform.rotation);
        Debug.Log("Spawn " + _enemyMax);



    }

    private Vector3 DondeSpawnear()
    {
        GameObject[] Blokes = GameObject.FindGameObjectsWithTag("Bloke");
        GameObject Bloke = Blokes[Random.Range(0, Blokes.Length)];
        Bounds bounds = MaximBounds(Bloke);
        do
        {
            float spawnX = bounds.center.x + Random.Range(-bounds.extents.x / 2, bounds.extents.x / 2);
            float spawnY = bounds.center.y + Random.Range(-bounds.extents.y / 2, bounds.extents.y / 2);
            _posicionInicial = new Vector2(spawnX, spawnY);
        }
        while (Physics2D.CircleCast(_posicionInicial, 1, Vector3.forward).collider != null);

        return _posicionInicial;
    }
    private Bounds MaximBounds(GameObject go)
    {
        var renderers = go.GetComponentsInChildren<Renderer>();
        if (renderers.Length == 0) return new Bounds(go.transform.position, Vector3.zero);
        var bounds = renderers[0].bounds;
        foreach (Renderer render in renderers)
        {
            bounds.Encapsulate(render.bounds);
        }
        return bounds;
    }

}
