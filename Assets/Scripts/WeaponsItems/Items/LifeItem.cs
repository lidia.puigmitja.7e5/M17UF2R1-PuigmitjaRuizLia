using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LifeItem : Items
{
    private float _vidaExtra;

    void Start()
    {
        _vidaExtra = 10;
    }

    protected override void Effecto(Collider2D collision)
    {
        GameManager.SetRaveCoins(10);
        collision.gameObject.GetComponent<Player>().ItHeals(_vidaExtra);
    }

  
}
