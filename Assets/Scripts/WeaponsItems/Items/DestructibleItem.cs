using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class DestructibleItem : MonoBehaviour
{

    private float _count = 0;
    [SerializeField] private GameObject[] prefabMunicion = new GameObject[6];
    // Start is called before the first frame update


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (ControladorCicloDia.CicloDias == CiclosDias.Dia && GameManager.EstadoJuego == EstadoJuego.Running)
        {
            switch (collision.gameObject.tag)
            {
                case "bBombeta":
                    _count += 5f;
                    break;
                case "bTriposa":
                    _count += 4f;
                    break;
                case "bSpeed":
                    _count += 3f;
                    break;
                case "bXanax":
                    _count += 3f;
                    break;
                case "bAntiPsik":
                    _count += 3f;
                    break;
            }

        }
        if (_count >= 10)
        {
            Destroy(gameObject);
            int num = Random.Range(0, prefabMunicion.Length);
            if (num != prefabMunicion.Length)
            {
                Instantiate(prefabMunicion[num], transform.position, transform.rotation);
            }

            _count = 0;
        }

    }
}
