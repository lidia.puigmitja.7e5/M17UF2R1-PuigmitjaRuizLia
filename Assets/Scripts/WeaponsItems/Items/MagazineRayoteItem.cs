using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazineRayoteItem : MagazineItems
{
    protected override void Effecto(Collider2D collision)
    {
        GameManager.SetRaveCoins(5);
        weaponControl = collision.transform.GetChild(0).GetComponent<WeaponControl>();
        if (weaponControl.GetCurrentState() == weapon)
            weaponControl.weapon.Charge();
        else
            if (weaponControl.WeaponList.SpeedTruenoValues.totalMagazine < weaponControl.WeaponList.SpeedTruenoValues.totalMagazineWeapon) weaponControl.WeaponList.SpeedTruenoValues.totalMagazine += 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        weapon = WeaponControl.EnumWeapons.SpeedTrueno;
    }

  
}
