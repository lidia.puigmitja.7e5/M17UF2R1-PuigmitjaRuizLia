using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazinePiumPiumItem : MagazineItems
{
    protected override void Effecto(Collider2D collision)
    {
        GameManager.SetRaveCoins(5);

        weaponControl = collision.transform.GetChild(0).GetComponent<WeaponControl>();
        if (weaponControl.GetCurrentState() == weapon)
            weaponControl.weapon.Charge();
        else
            if (weaponControl.WeaponList.BenzaXanaXValues.totalMagazine < weaponControl.WeaponList.BenzaXanaXValues.totalMagazineWeapon) weaponControl.WeaponList.BenzaXanaXValues.totalMagazine += 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        weapon = WeaponControl.EnumWeapons.BenzaXanaX;
    }

  
}
