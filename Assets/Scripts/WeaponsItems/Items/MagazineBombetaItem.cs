using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazineBombetaItem : MagazineItems
{
    protected override void Effecto(Collider2D collision)
    {
        GameManager.SetRaveCoins(7);

        weaponControl = collision.transform.GetChild(0).GetComponent<WeaponControl>();
        if (weaponControl.GetCurrentState() == weapon)
            weaponControl.weapon.Charge();
        else
            if(weaponControl.WeaponList.MDM4morValues.totalMagazine< weaponControl.WeaponList.MDM4morValues.totalMagazineWeapon) weaponControl.WeaponList.MDM4morValues.totalMagazine += 1;

        
    }

    // Start is called before the first frame update
    void Start()
    {
        weapon = WeaponControl.EnumWeapons.MDM4mor;
    }

}
