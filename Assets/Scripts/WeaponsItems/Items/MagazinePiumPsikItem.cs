using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazinePiumPsikItem : MagazineItems
{
    protected override void Effecto(Collider2D collision)
    {
        GameManager.SetRaveCoins(5);
        weaponControl = collision.transform.GetChild(0).GetComponent<WeaponControl>();
        if (weaponControl.GetCurrentState() == weapon)
            weaponControl.weapon.Charge();
        else
            if (weaponControl.WeaponList.AntiPsikotikValues.totalMagazine < weaponControl.WeaponList.AntiPsikotikValues.totalMagazineWeapon) weaponControl.WeaponList.AntiPsikotikValues.totalMagazine += 1;
    
}

    // Start is called before the first frame update
    void Start()
    {
        weapon = WeaponControl.EnumWeapons.AntiPsikotik;
    }

   
}
