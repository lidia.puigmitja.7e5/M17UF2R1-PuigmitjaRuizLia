using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagazinePotadaItem : MagazineItems
{
    protected override void Effecto(Collider2D collision)
    {
        GameManager.SetRaveCoins(7);
        weaponControl = collision.transform.GetChild(0).GetComponent<WeaponControl>();
        if (weaponControl.GetCurrentState() == weapon)
            weaponControl.weapon.Charge();
        else
            if (weaponControl.WeaponList.TriposArcoirisValues.totalMagazine < weaponControl.WeaponList.TriposArcoirisValues.totalMagazineWeapon) weaponControl.WeaponList.TriposArcoirisValues.totalMagazine += 1;
    }

    // Start is called before the first frame update
    void Start()
    {
        weapon = WeaponControl.EnumWeapons.TriposArcoiris;
    }

 
}
