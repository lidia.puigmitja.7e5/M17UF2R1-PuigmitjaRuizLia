using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponControl : MonoBehaviour
{
   
    public WeaponList WeaponList;

    public enum EnumWeapons
    {
        TriposArcoiris,
        SpeedTrueno,
        MDM4mor,
        BenzaXanaX,
        AntiPsikotik
    }

    [HideInInspector] public Weapon weapon;

    private EnumWeapons weaponEnum;
    private int indiceWeaponActual;
    private void Awake()
    {
        ResetPlayerWeaponsSOValues();
    }

    public void ResetPlayerWeaponsSOValues()
    {

        WeaponList.TriposArcoirisValues.SetValues();
        WeaponList.SpeedTruenoValues.SetValues();
        WeaponList.BenzaXanaXValues.SetValues();
        WeaponList.MDM4morValues.SetValues();
        WeaponList.AntiPsikotikValues.SetValues();
    }

    // Start is called before the first frame update
    void Start()
    {
        indiceWeaponActual = 0;

         weaponEnum = EnumWeapons.TriposArcoiris;
       
        TransitionToState();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f || Input.GetKeyDown(KeyCode.C)) // forward
        {
            if (indiceWeaponActual < 4)
            {


                indiceWeaponActual++;
                RegisterStateData();
                ChangeWeaponEnum();
                TransitionToState();

            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0f || Input.GetKeyDown(KeyCode.X)) // backwards
        {
            if (indiceWeaponActual > 0)
            {

                indiceWeaponActual--;
                RegisterStateData();
                ChangeWeaponEnum();
                TransitionToState();

            }

        }

        weapon.Shoot();

    }

    void ChangeWeaponEnum()
    {
        switch (indiceWeaponActual)
        {
            case 0:
                weaponEnum = EnumWeapons.TriposArcoiris;
                break;
            case 1:
                weaponEnum = EnumWeapons.SpeedTrueno;
                break;
            case 2:
                weaponEnum = EnumWeapons.MDM4mor;
                break;
            case 3:
                weaponEnum = EnumWeapons.BenzaXanaX;
                break;
            case 4:
                weaponEnum = EnumWeapons.AntiPsikotik;
                break;
        }
    }



    void TransitionToState()
    {
        Transform weaponGO = this.gameObject.transform;

        switch (weaponEnum)
        {
            case EnumWeapons.TriposArcoiris:
                weapon = gameObject.AddComponent<TriposArcoiris>();
                break;
            case EnumWeapons.SpeedTrueno:
                weapon = gameObject.AddComponent<SpeedTrueno>();               
                break;
            case EnumWeapons.MDM4mor:
                weapon = gameObject.AddComponent<MDM4mor>();
                break;
            case EnumWeapons.BenzaXanaX:
                weapon = gameObject.AddComponent<BenzaXanaX>();
                break;
            case EnumWeapons.AntiPsikotik:
                weapon = gameObject.AddComponent<AntiPsikotik>();
                break;
        }

        weapon.SetValues(this);
        weaponGO.GetComponent<SpriteRenderer>().sprite = weapon.GetSprite().sprite;
      
    }

    void RegisterStateData()
    {
        weapon.RegisterValues(this);
        Destroy(weapon);
    }

    public EnumWeapons GetCurrentState() => weaponEnum;


}
