using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiPsikotik : Weapon
{
  

    public override void SetValues(WeaponControl weaponControl)
    {
        bulletPrefab = weaponControl.WeaponList.AntiPsikotikValues.bulletPrefab;
        weaponPrefab = weaponControl.WeaponList.AntiPsikotikValues.weaponPrefab;
        weaponName = weaponControl.WeaponList.AntiPsikotikValues.weaponName;

        totalBullets = weaponControl.WeaponList.AntiPsikotikValues.totalBullets;
        totalMagazine = weaponControl.WeaponList.AntiPsikotikValues.totalMagazine;
        totalMagazineWeapon = weaponControl.WeaponList.AntiPsikotikValues.totalMagazineWeapon;
        magazineCapacity = weaponControl.WeaponList.AntiPsikotikValues.magazineCapacity;
        reloadSpeed = weaponControl.WeaponList.AntiPsikotikValues.reloadSpeed;
        fireRate = weaponControl.WeaponList.AntiPsikotikValues.fireRate;
        speedBullets = weaponControl.WeaponList.AntiPsikotikValues.speedBullets;
    }
    public override void RegisterValues(WeaponControl weaponControl)
    {
        weaponControl.WeaponList.AntiPsikotikValues.bulletPrefab = bulletPrefab;
        weaponControl.WeaponList.AntiPsikotikValues.weaponPrefab = weaponPrefab;
        weaponControl.WeaponList.AntiPsikotikValues.weaponName = weaponName;

        weaponControl.WeaponList.AntiPsikotikValues.totalBullets = totalBullets;
        weaponControl.WeaponList.AntiPsikotikValues.totalMagazine = totalMagazine;
        weaponControl.WeaponList.AntiPsikotikValues.totalMagazineWeapon = totalMagazineWeapon;
        weaponControl.WeaponList.AntiPsikotikValues.magazineCapacity = magazineCapacity;
        weaponControl.WeaponList.AntiPsikotikValues.reloadSpeed = reloadSpeed;
        weaponControl.WeaponList.AntiPsikotikValues.fireRate = fireRate;
        weaponControl.WeaponList.AntiPsikotikValues.speedBullets = speedBullets;
    }

    
}
