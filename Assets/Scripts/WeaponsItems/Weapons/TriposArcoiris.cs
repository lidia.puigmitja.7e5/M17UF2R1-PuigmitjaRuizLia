using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriposArcoiris : Weapon
{


    public override void SetValues(WeaponControl weaponControl)
    {
        bulletPrefab = weaponControl.WeaponList.TriposArcoirisValues.bulletPrefab;
        weaponPrefab = weaponControl.WeaponList.TriposArcoirisValues.weaponPrefab;
        weaponName = weaponControl.WeaponList.TriposArcoirisValues.weaponName;

        totalBullets = weaponControl.WeaponList.TriposArcoirisValues.totalBullets;
        totalMagazine = weaponControl.WeaponList.TriposArcoirisValues.totalMagazine;
        totalMagazineWeapon = weaponControl.WeaponList.TriposArcoirisValues.totalMagazineWeapon;
        magazineCapacity = weaponControl.WeaponList.TriposArcoirisValues.magazineCapacity;
        reloadSpeed = weaponControl.WeaponList.TriposArcoirisValues.reloadSpeed;
        fireRate = weaponControl.WeaponList.TriposArcoirisValues.fireRate;
        speedBullets = weaponControl.WeaponList.TriposArcoirisValues.speedBullets;


    }
    public override void RegisterValues(WeaponControl weaponControl)
    {
        weaponControl.WeaponList.TriposArcoirisValues.bulletPrefab = bulletPrefab;
        weaponControl.WeaponList.TriposArcoirisValues.weaponPrefab = weaponPrefab;
        weaponControl.WeaponList.TriposArcoirisValues.weaponName = weaponName;

        weaponControl.WeaponList.TriposArcoirisValues.totalBullets = totalBullets;
        weaponControl.WeaponList.TriposArcoirisValues.totalMagazine = totalMagazine;
        weaponControl.WeaponList.TriposArcoirisValues.totalMagazineWeapon = totalMagazineWeapon;
        weaponControl.WeaponList.TriposArcoirisValues.magazineCapacity = magazineCapacity;
        weaponControl.WeaponList.TriposArcoirisValues.reloadSpeed = reloadSpeed;
        weaponControl.WeaponList.TriposArcoirisValues.fireRate = fireRate;
        weaponControl.WeaponList.TriposArcoirisValues.speedBullets = speedBullets;
    }


}
