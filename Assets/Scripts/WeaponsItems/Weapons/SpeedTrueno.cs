using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedTrueno : Weapon
{

    public override void SetValues(WeaponControl weaponControl)
    {
        bulletPrefab = weaponControl.WeaponList.SpeedTruenoValues.bulletPrefab;
        weaponPrefab = weaponControl.WeaponList.SpeedTruenoValues.weaponPrefab;
        weaponName = weaponControl.WeaponList.SpeedTruenoValues.weaponName;

        totalBullets = weaponControl.WeaponList.SpeedTruenoValues.totalBullets;
        totalMagazine = weaponControl.WeaponList.SpeedTruenoValues.totalMagazine;
        totalMagazineWeapon = weaponControl.WeaponList.SpeedTruenoValues.totalMagazineWeapon;
        magazineCapacity = weaponControl.WeaponList.SpeedTruenoValues.magazineCapacity;
        reloadSpeed = weaponControl.WeaponList.SpeedTruenoValues.reloadSpeed;
        fireRate = weaponControl.WeaponList.SpeedTruenoValues.fireRate;
        speedBullets = weaponControl.WeaponList.SpeedTruenoValues.speedBullets;
    }
    public override void RegisterValues(WeaponControl weaponControl)
    {
        weaponControl.WeaponList.SpeedTruenoValues.bulletPrefab = bulletPrefab;
        weaponControl.WeaponList.SpeedTruenoValues.weaponPrefab = weaponPrefab;
        weaponControl.WeaponList.SpeedTruenoValues.weaponName = weaponName;

        weaponControl.WeaponList.SpeedTruenoValues.totalBullets = totalBullets;
        weaponControl.WeaponList.SpeedTruenoValues.totalMagazine = totalMagazine;
        weaponControl.WeaponList.SpeedTruenoValues.totalMagazineWeapon = totalMagazineWeapon;
        weaponControl.WeaponList.SpeedTruenoValues.magazineCapacity = magazineCapacity;
        weaponControl.WeaponList.SpeedTruenoValues.reloadSpeed = reloadSpeed;
        weaponControl.WeaponList.SpeedTruenoValues.fireRate = fireRate;
        weaponControl.WeaponList.SpeedTruenoValues.speedBullets = speedBullets;
    }

}
