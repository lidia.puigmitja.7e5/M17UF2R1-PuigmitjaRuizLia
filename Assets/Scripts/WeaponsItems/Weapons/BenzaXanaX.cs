using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BenzaXanaX : Weapon
{
  

    public override void SetValues(WeaponControl weaponControl)
    {
        bulletPrefab = weaponControl.WeaponList.BenzaXanaXValues.bulletPrefab;
        weaponPrefab = weaponControl.WeaponList.BenzaXanaXValues.weaponPrefab;
        weaponName = weaponControl.WeaponList.BenzaXanaXValues.weaponName;

        totalBullets = weaponControl.WeaponList.BenzaXanaXValues.totalBullets;
        totalMagazine = weaponControl.WeaponList.BenzaXanaXValues.totalMagazine;
        totalMagazineWeapon = weaponControl.WeaponList.BenzaXanaXValues.totalMagazineWeapon;
        magazineCapacity = weaponControl.WeaponList.BenzaXanaXValues.magazineCapacity;
        reloadSpeed = weaponControl.WeaponList.BenzaXanaXValues.reloadSpeed;
        fireRate = weaponControl.WeaponList.BenzaXanaXValues.fireRate;
        speedBullets = weaponControl.WeaponList.BenzaXanaXValues.speedBullets;
    }
    public override void RegisterValues(WeaponControl weaponControl)
    {
        weaponControl.WeaponList.BenzaXanaXValues.bulletPrefab = bulletPrefab;
        weaponControl.WeaponList.BenzaXanaXValues.weaponPrefab = weaponPrefab;
        weaponControl.WeaponList.BenzaXanaXValues.weaponName = weaponName;

        weaponControl.WeaponList.BenzaXanaXValues.totalBullets = totalBullets;
        weaponControl.WeaponList.BenzaXanaXValues.totalMagazine = totalMagazine;
        weaponControl.WeaponList.BenzaXanaXValues.totalMagazineWeapon = totalMagazineWeapon;
        weaponControl.WeaponList.BenzaXanaXValues.magazineCapacity = magazineCapacity;
        weaponControl.WeaponList.BenzaXanaXValues.reloadSpeed = reloadSpeed;
        weaponControl.WeaponList.BenzaXanaXValues.fireRate = fireRate;
        weaponControl.WeaponList.BenzaXanaXValues.speedBullets = speedBullets;
    }

}
