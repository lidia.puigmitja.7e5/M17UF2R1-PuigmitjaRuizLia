using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MDM4mor : Weapon
{
 

    public override void SetValues(WeaponControl weaponControl)
    {
        bulletPrefab = weaponControl.WeaponList.MDM4morValues.bulletPrefab;
        weaponPrefab = weaponControl.WeaponList.MDM4morValues.weaponPrefab;
        weaponName = weaponControl.WeaponList.MDM4morValues.weaponName;

        totalBullets = weaponControl.WeaponList.MDM4morValues.totalBullets;
        totalMagazine = weaponControl.WeaponList.MDM4morValues.totalMagazine;
        totalMagazineWeapon = weaponControl.WeaponList.MDM4morValues.totalMagazineWeapon;
        magazineCapacity = weaponControl.WeaponList.MDM4morValues.magazineCapacity;
        reloadSpeed = weaponControl.WeaponList.MDM4morValues.reloadSpeed;
        fireRate = weaponControl.WeaponList.MDM4morValues.fireRate;
        speedBullets = weaponControl.WeaponList.MDM4morValues.speedBullets;
    }
    public override void RegisterValues(WeaponControl weaponControl)
    {
        weaponControl.WeaponList.MDM4morValues.bulletPrefab = bulletPrefab;
        weaponControl.WeaponList.MDM4morValues.weaponPrefab = weaponPrefab;
        weaponControl.WeaponList.MDM4morValues.weaponName = weaponName;

        weaponControl.WeaponList.MDM4morValues.totalBullets = totalBullets;
        weaponControl.WeaponList.MDM4morValues.totalMagazine = totalMagazine;
        weaponControl.WeaponList.MDM4morValues.totalMagazineWeapon = totalMagazineWeapon;
        weaponControl.WeaponList.MDM4morValues.magazineCapacity = magazineCapacity;
        weaponControl.WeaponList.MDM4morValues.reloadSpeed = reloadSpeed;
        weaponControl.WeaponList.MDM4morValues.fireRate = fireRate;
        weaponControl.WeaponList.MDM4morValues.speedBullets = speedBullets;
    }

}
