using System.Collections;
using System.Collections.Generic;
using Unity.Burst.Intrinsics;
using UnityEngine;
using UnityEngine.TextCore.Text;

public class Weapon : MonoBehaviour
{

    public Projectil bulletPrefab; // Stores out Bullet Prefab
    public GameObject weaponPrefab;

    public string weaponName;       // weapon name e.g  plasma cannon

    public int totalBullets;           // amount of bullets I have e.g. 5
    public int totalMagazine;           // amount of magazine I have e.g. 5
    public int totalMagazineWeapon;    // amount of magazine per weapon e.g. 5
    public int magazineCapacity;    // amount of bullets per magazine e.g. 5
    public float reloadSpeed;       // time to reload   e.g 2.5f (2.5 seconds)
    public float fireRate;
    public float speedBullets;


    public float IntervaloSpawneo;


    public SpriteRenderer spriteRenderer;



    private bool ShootFireClick()
    {

        if (IntervaloSpawneo > 0)
        {
            IntervaloSpawneo -= Time.deltaTime;
            return false;
        }
        else
        {

            return true;
        }

    }

    public void Shoot()
    {
        if (GameManager.EstadoJuego == EstadoJuego.Running)
        {
            if (totalBullets > 0)
            {

                if (ShootFireClick())
                {
                    if (Input.GetMouseButton(0))
                    {
                        Transform weaponGO = this.gameObject.transform.GetChild(0);
                        Projectil projectil = Instantiate(this.bulletPrefab, weaponGO.transform.position, transform.rotation);
                        projectil.LaunchProjectil(-transform.up, speedBullets);
                        totalBullets--;
                        IntervaloSpawneo = fireRate;
                    }

                }

            }
            else
            {
                Reload();
            }
        }


    }
    public void Reload()
    {

        if (ShootFireClick())
            if (totalMagazine > 0)
            {
                if (totalBullets == 0)
                {

                    totalMagazine -= 1;
                    totalBullets = magazineCapacity;
                    IntervaloSpawneo = reloadSpeed;
                }

            }



    }




    public virtual void SetValues(WeaponControl weaponControl) { }

    public virtual void RegisterValues(WeaponControl weaponControl) { }

    public virtual SpriteRenderer GetSprite()
    {


        spriteRenderer = weaponPrefab.GetComponent<SpriteRenderer>();
        return spriteRenderer;
    }

    public int GetTotalBullets() => totalBullets;

    public int GetTotalMagazine() => totalMagazine;

    public int Charge()
    {
        if (totalMagazine < totalMagazineWeapon) totalMagazine += 1;

        return totalMagazine;
    }

}
