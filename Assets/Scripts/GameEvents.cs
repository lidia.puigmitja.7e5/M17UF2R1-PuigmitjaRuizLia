using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents current;
    // Start is called before the first frame update
    void Awake()
    {
        if (current != null && current != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            current = this;
        }
    }

    public delegate void KilledEnemies();
    public static event KilledEnemies KilledAllEnemies;


    private void Update()
    {
        if (GameManager.Enemies == 0 && KilledAllEnemies != null)
        {
            KilledAllEnemies();
        }

    }


}


