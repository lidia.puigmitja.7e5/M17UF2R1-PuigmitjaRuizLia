using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class SaveResultGame : MonoBehaviour
{
    
    void Awake()
    {
        
        string path = Application.dataPath + "/StreamingAssets/ranking.json";
        bool filexists1 = ResultSave.IfFileExists(path);
        string testDate = ResultSave.GetDateTimeNow();
        if (filexists1 == false)
            ResultSave.CreateJsonFile(testDate, path);

        ResultSave.AddTextToJsonFile(testDate, path);
        
    }

}

class ResultSave
{


    //Funci�: Crear un arxiu de tipus json sense contingut.
    //Par�metres: string amb la ruta del fitxer.
    public static void CreateJsonFile(string testDate, string path)
    {
        var user1 = new User
        {
            Name = "ROOT",
            Puntuacio = 000,
            TestDate = testDate
        };

        string json1 = JsonConvert.SerializeObject(user1);

        using (StreamWriter sw = new StreamWriter(path))
        {
            sw.WriteLine(json1);
        }
      
    }


    //Funci�: Afegir contingut al fitxer json.
    //Par�metres: string amb el nom de l'usuari, int amb la puntuaci�, string amb el tipus de daltonisme, string amb la ruta del fitxer.
    public static void AddTextToJsonFile(string testDate, string path)
    {
        var user1 = new User
        {
            Name = GameManager.PlayerName,
            Puntuacio = GameManager.RaveCoins,
            TestDate = testDate
        };

        string json1 = JsonConvert.SerializeObject(user1);
        File.AppendAllText(path, json1 + "\n");
    }




    //Funci�: Comprobar si el fitxer indicat existeix.
    //Par�metres: string amb la ruta del fitxer.
    //Retorna: true si existeix o flase si no existeix.
    public static bool IfFileExists(string path)
    {
        if (File.Exists(path))
        {
            return true;
        }
        else
        {
            Debug.Log("Error. File doesn't exists on current path.");
            return false;
        }
        return false;
    }


    //Funci�: Funci� que obt� la data i hora actuals.
    //Retorna: true un string amb la data i hora actuals.
    public static string GetDateTimeNow()
    {
        DateTime now = DateTime.Now.Date;
        string todayDate = now.ToString();
        //Console.WriteLine(todayDate);
        return todayDate;
    }


}

 public class User
{
    public string Name { get; set; }
    public int Puntuacio { get; set; }
    public string TestDate { get; set; }

  
}