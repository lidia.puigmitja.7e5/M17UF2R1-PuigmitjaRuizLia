using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class ShowResultGames : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
        string path = Application.dataPath + "/StreamingAssets/ranking.json";
        bool filexists1 = ResultSave.IfFileExists(path);
        if (filexists1 == true)
        {
            string[] lastHistory = ShowHistory.SelectLastGame(path);
            string[] bestHistory = ShowHistory.SelectBestGame(path);
            GameObject[] bestFilas = {
            GameObject.Find("BestName"), GameObject.Find("BestPuntuation"),
            GameObject.Find("BestTime")
        };
            GameObject[] lastFilas = {
            GameObject.Find("LastName"), GameObject.Find("LastPuntuation"),
            GameObject.Find("LastTime")
        };
            for (int i = 0; i < bestHistory.Length; i++)
            {
                bestFilas[i].GetComponent<TMP_Text>().text = bestHistory[i];
            }
            for (int i = 0; i < lastHistory.Length; i++)
            {
                lastFilas[i].GetComponent<TMP_Text>().text = lastHistory[i];
            }

        }
       
        
    
    }

}

class ShowHistory
{
    public static string[] SelectBestGame(string path)
    {
        List<User> user1 = new List<User>();
        
        using (StreamReader jsonStream = File.OpenText(path))
        {
            string line;
            while ((line = jsonStream.ReadLine()) != null)
            {
                User user = JsonConvert.DeserializeObject<User>(line);
                user1.Add(user);
            }
        }
        User user2 = user1[0];
        int maxPuntuation=0;
        for (int i = 0; i < user1.Count; i++)
        {
            if (user1[i].Puntuacio > maxPuntuation)
            {
                maxPuntuation = user1[i].Puntuacio;
                user2 = user1[i];
            }
        }

       int lastPuntuation = user2.Puntuacio;
        string lastName = user2.Name;
        string lastTimeGame = user2.TestDate;

        string[] LastGame = new string[] { lastName, lastPuntuation.ToString(), lastTimeGame };

        return LastGame;
    }

    public static string[] SelectLastGame(string path)
    {
        List<User> user1 = new List<User>();
        
        using (StreamReader jsonStream = File.OpenText(path))
        {
            string line;
            while ((line = jsonStream.ReadLine()) != null)
            {
                User user = JsonConvert.DeserializeObject<User>(line);
                user1.Add(user);
            }
        }
        User user2 = user1[user1.Count-1];

        int bestPuntuation = user2.Puntuacio;
        string bestName = user2.Name;
        string bestTimeGame = user2.TestDate;

        string[] bestGame = new string[] { bestName, bestPuntuation.ToString(), bestTimeGame };

        return bestGame;
    }

    public static int SelectRaveCoins(string path)
    {
        
        List<User> user1 = new List<User>();

        using (StreamReader jsonStream = File.OpenText(path))
        {
            string line;
            while ((line = jsonStream.ReadLine()) != null)
            {
                User user = JsonConvert.DeserializeObject<User>(line);
                user1.Add(user);
            }
        }
        foreach (User user in user1)
        {
            if (user.Name == GameManager.PlayerName)
            {
                return user.Puntuacio;
            }
            else
            {
               return 0;
            }
        }
        
        return 0;

    }
}