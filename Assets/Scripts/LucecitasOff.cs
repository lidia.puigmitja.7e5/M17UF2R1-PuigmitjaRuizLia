using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class LucecitasOff : MonoBehaviour
{

    void Start()
    {
        ControladorCicloDia.LightOfff += CloseLight;

    }

    // Update is called once per frame
    void CloseLight()
    {
        this.gameObject.GetComponent<Light2D>().intensity = 0;
    }

    private void OnDisable()
    {
        ControladorCicloDia.LightOfff -= CloseLight;

    }
}
