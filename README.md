# CARE-ЯAVƎ
Care-Rave es un rogue like ambientado en el bosque, su temática y mecánicas van dirijidas a la conscienciación de pasarlo bien tomando precauciones y cuidandose a uno mismo y a los demás, por lo que no es un juego violento como tal.   
El personaje se encuentra con ganas de fiesta y decide ir a una Rave.  
De dia tiene que tener cura de si mismo, encontrar los items necesarios para hacer frente a una noche de diversión.  
Estos items serian: tiendaDeAcampada, Aguita, Zumito, Chocolate, PerritoCaliente, VitaminaC.
De noche tiene que cuidar a los demás colegas que se han pasado con las dosis o se han quedado cortos.  
Para ello les puede subministrar 5 diferentes sustancias, y teniendo en cuenta el problema del colega le hará más efecto o menos. 
- Substancias:  Antipsikotik, benzaXanaX, MDM4mor, SpeedTrueno, TriposArcoiris.
- PunkySad: Te lanza lagrimas que te duelen en el alma, incapaz de moverse de la tristeza. 
  - Efecto sustancias de mayor a menor: MDM4mor, TriposArcoiris, SpeedTrueno, Antipsikotik, benzaXanaX.
- ParanoidScream: Te persigue en plena paranoia y te chilla, su chillido es tan ensordecedor que te destroza por dentro.
  - Efecto sustancias de mayor a menor: Antipsikotik, benzaXanaX, MDM4mor, TriposArcoiris, SpeedTrueno.
- (Aun por implementar) Anxiety -pinxazos por doquier  
- (Aun por implementar) Arroweed -te realentizará  

Los niveles se generan a medida que vas ayudando a estos colegas y a ti mismo a disfrutar de la mejor rave de vuestras vidas mientras suena DJPipiolo con temas de la ruta del bakalao.

(Aun por implementar) Cuando ayudas a los colegas, esos se van a bailar enfrente del bafle.
Hay una tienda donde comprar items que no encuentras de dia en ningún sitio.
El panel de ayuda te expondrá que hace cada item: alguno te amuenta la vida, otro te hace ir más rápido, realentiza a los punkis ansiosos, otra te hace inmune algún tiempo..
Hay panel de configuración de volumen.
Si dejas que los problemas de tus colegas te superen, si no consigues ayudarlos, si no te cuidas, etc tu barra de vida disminuirá hasta agotarse y entonces, y solo entonces querido mio, habrás perdido.

## Controles:

Movimiento: WASD /flechas 🠹🠸🠻🠺  
Disparar: click izquierdo ratón  
Dash: Espacio /Shift Derecho  
Cambiar arma: rueda de ratón o XC  
